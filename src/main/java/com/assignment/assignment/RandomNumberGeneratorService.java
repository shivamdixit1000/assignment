package com.assignment.assignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

@Service
public class RandomNumberGeneratorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomNumberGeneratorService.class);
    public static final int RANDOM_INTEGER_BOUNDARY = 100000;
    private final List<Integer> queue = new ArrayList<>();
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private final Random random = new Random();

    public RandomNumberGeneratorService() {
        Runnable task = () -> {
            List<Integer> list = randomNumberList();
            queue.addAll(list);
        };
        executorService.scheduleAtFixedRate(task, 5, 1, TimeUnit.SECONDS);
    }

    private List<Integer> randomNumberList() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            result.add(random.nextInt(RANDOM_INTEGER_BOUNDARY));
        }
        queue.addAll(result);
        LOGGER.info("New set of Random numbers generated:: {}", result);
        return new ArrayList<>();
    }

    public List<Integer> getQueue() {
        return queue;
    }

}
