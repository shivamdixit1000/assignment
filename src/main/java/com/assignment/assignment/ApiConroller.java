package com.assignment.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("rest")
public class ApiConroller {

    @Autowired
    RandomNumbersCumulationService randomNumbersCumulationService;

    @GetMapping(value = "/{minutes}", produces = "application/json")
    public List<Integer> getCumulativeResult(@PathVariable int minutes) {
        return randomNumbersCumulationService.getCumulativeNumbers(minutes);
    }


}
