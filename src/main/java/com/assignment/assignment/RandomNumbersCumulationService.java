package com.assignment.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RandomNumbersCumulationService {
    RandomNumberGeneratorService randomNumberGeneratorService;

    @Autowired
    public RandomNumbersCumulationService(RandomNumberGeneratorService randomNumberGeneratorService) {
        this.randomNumberGeneratorService = randomNumberGeneratorService;
    }

    public List<Integer> getCumulativeNumbers(int mins) {
        List<Integer> result = new ArrayList<>();
        int count = 60*mins*5;
        List<Integer> list = randomNumberGeneratorService.getQueue();
        for(int i = 0; i < count && i < list.size(); i++) {
            Integer item = list.get(i);
            result.add(item);
        }
        return result;
    }
}
