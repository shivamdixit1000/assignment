#How to Build the project
Build the Spring Boot project using maven command -> "mvn clean install"

# How to launch the project
Launch AssignmentApplication (main Spring Boot class) using IDE
After this Spring Boot App autostarts

#Where does app launch automatically?
http://localhost:8081/

# Where is Source code for Program 1?****
Source code for Program 1 is in class "RandomNumberGeneratorService.java"


# Where to see Program 1 output? The 5 random numbers, generated every 5 seconds
After the spring boot app launches, you will find random numbers being outputted both to the Console and
the log file "random-numbers.log" 

# Source code for Program 2?
Source code for Program 2 is in class "RandomNumbersCumulationService.java"

# Rest end point to see the Output of Program2?
At any point when app is running, hit the following URL in Browser
"http://localhost:8081/rest/{min}"

Examples for rest endpoint: 
http://localhost:8081/rest/5   -> for getting cumulative numbers in last 5 mins
http://localhost:8081/rest/1        -> for getting cumulative numbers in last 1 min 


#Assumptions:
->Random numbers generated are integers from 1 to 100000
->Spring Boot App keeps on running until manually stopped by user






 